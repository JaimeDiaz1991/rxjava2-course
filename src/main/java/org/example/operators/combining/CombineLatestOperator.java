package org.example.operators.combining;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CombineLatestOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(CombineLatestOperator.class);

    /**
     * cuando un elemento es emitido por cualquiera de dos Observables,
     * combine el último elemento emitido por cada Observable a través de una función específica
     * y emita elementos basados en los resultados de esta función
     *
     * El operador CombineLatest se comporta de manera similar a Zip,
     * pero mientras que Zip emite elementos solo cuando cada uno de los Observables de origen comprimido han emitido un elemento previamente descomprimido,
     * CombineLatest emite un elemento siempre que cualquiera de los Observables de origen emite un elemento
     * (siempre que cada uno de los la fuente Observables ha emitido al menos un elemento).
     * Cuando cualquiera de los Observables de origen emite un elemento,
     * CombineLatest combina los elementos emitidos más recientemente de cada uno de los otros Observables de origen,
     * utilizando una función que usted proporcione, y emite el valor de retorno de esa función.
     **/
    public static void main(String[] args) {
        LOGGER.info("CombineLatest Operator");
        Observable observable = Observable.interval(1000, TimeUnit.MILLISECONDS);
        Observable observable1 = Observable.interval(2000, TimeUnit.MILLISECONDS);
        Observable.combineLatest(observable,observable1,(o1,o2)-> "first: "+o1.toString() + " second "+ o2.toString()).take(6).subscribe(new DemoObserver());
        RxUtils.sleep(10000L);

    }
}
