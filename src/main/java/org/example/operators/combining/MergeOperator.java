package org.example.operators.combining;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.concurrent.TimeUnit;

public class MergeOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(MergeOperator.class);

    /**
     combinar múltiples Observables en uno fusionando sus emisiones
     Puede combinar la salida de varios Observables para que actúen como un solo Observable, utilizando el operador Fusionar.
     Merge puede intercalar los elementos emitidos por los Observables fusionados (un operador similar, Concat, no entrelaza elementos, pero emite todos los elementos de Observable de cada fuente a su vez antes de comenzar a emitir elementos de la siguiente fuente Observable).

     Una notificación de error de cualquiera de los Observables de origen se transmitirá inmediatamente a los observadores y finalizará el Observable fusionado.
     **/
    public static void main(String[] args) {
        LOGGER.info("Merge Operator");
        Observable observable = Observable.fromIterable(RxUtils.shapes(5));
        Observable observable1 = Observable.fromIterable(RxUtils.postiveNumbers(5));
        Observable.merge(observable,observable1).subscribe(new DemoObserver());
        RxUtils.sleep(10000L);

    }
}
