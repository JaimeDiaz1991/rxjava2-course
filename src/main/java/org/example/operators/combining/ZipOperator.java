package org.example.operators.combining;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

public class ZipOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ZipOperator.class);

    /**
     combinar las emisiones de varios Observables a través de una función específica
     y emitir elementos individuales para cada combinación en función de los resultados de esta función
     **/
    public static void main(String[] args) {
        LOGGER.info("Zip Operator");
        Observable shapes = Observable.fromIterable(RxUtils.shapes(5));
        Observable numbers = Observable.fromIterable(RxUtils.postiveNumbers(5));
        numbers.zipWith(shapes,(o1,o2)-> o1.toString() + ": "+ o2.toString()).subscribe(new DemoObserver());

    }
}
