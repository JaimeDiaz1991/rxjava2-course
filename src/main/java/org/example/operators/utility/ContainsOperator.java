package org.example.operators.utility;

import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.utils.RxUtils;

public class ContainsOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContainsOperator.class);

    /**
     determina si un Observable emite un elemento en particular o no
     **/
    public static void main(String[] args) {
        LOGGER.info("Contains Operator");
        Observable<Integer> positiveNumbers = Observable.fromIterable(RxUtils.postiveNumbers(10));
        positiveNumbers.contains(10)
                .subscribe(new SingleObserver<Boolean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull Boolean aBoolean) {
                        LOGGER.info("Some event is 10: "+ aBoolean);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });

    }
}
