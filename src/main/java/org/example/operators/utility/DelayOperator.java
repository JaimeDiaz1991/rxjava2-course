package org.example.operators.utility;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.concurrent.TimeUnit;

public class DelayOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DelayOperator.class);

    /**
     postponer las emisiones de un Observable hacia adelante en el tiempo en una cantidad especifica
     **/
    public static void main(String[] args) {
        LOGGER.info("Delay Operator");
        Observable numbers = Observable.fromIterable(RxUtils.postiveNumbers(5));
        numbers.delay(2, TimeUnit.SECONDS).subscribe(new DemoObserver());
        RxUtils.sleep(5000L);

    }
}
