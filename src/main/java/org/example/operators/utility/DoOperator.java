package org.example.operators.utility;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.concurrent.TimeUnit;

public class DoOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DoOperator.class);

    /**
     registrar una acción para realizar una variedad de eventos observables del ciclo de vida
     **/
    public static void main(String[] args) {
        LOGGER.info("Do Operator");
        Observable.fromIterable(RxUtils.shapes(10))
                .doOnSubscribe(disposable -> LOGGER.info("Stream is subscribed"))
                .doOnEach(shapeNotification -> LOGGER.info("current value "+ shapeNotification.getValue()))
                .doOnNext(shape -> LOGGER.info("Shape is "+shape))
                .doOnComplete(()->LOGGER.info("Completed"))
                .subscribe(new DemoObserver());

        RxUtils.sleep(5000L);

    }
}
