package org.example.operators.utility;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;

import java.util.concurrent.TimeUnit;

public class TakeUntilOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(TakeUntilOperator.class);

    /**
     descarta elementos emitidos por un Observable después de que un segundo Observable emite un elemento o termina
     **/
    public static void main(String[] args) {
        LOGGER.info("TakeUntil Operator");
        Observable singleSecond = Observable.interval(1, TimeUnit.SECONDS);
        Observable fiveSecond = Observable.interval(5, TimeUnit.SECONDS);
        singleSecond.takeUntil(fiveSecond).subscribe(new DemoObserver());

    }
}
