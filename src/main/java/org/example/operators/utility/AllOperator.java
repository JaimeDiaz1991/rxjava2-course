package org.example.operators.utility;

import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

public class AllOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(AllOperator.class);

    /**
     determina si todos los elementos emitidos por un Observable cumplen con algunos criterios
     **/
    public static void main(String[] args) {
        LOGGER.info("All Operator");
        Observable<Integer> positiveNumbers = Observable.fromIterable(RxUtils.postiveNumbers(10));
        positiveNumbers.all(integer -> integer>5)
                .subscribe(new SingleObserver<Boolean>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull Boolean aBoolean) {
                        LOGGER.info("Do all of the events are greater than 5: "+ aBoolean);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });

    }
}
