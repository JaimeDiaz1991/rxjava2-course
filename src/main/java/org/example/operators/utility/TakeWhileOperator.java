package org.example.operators.utility;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.concurrent.TimeUnit;

public class TakeWhileOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(TakeWhileOperator.class);

    /**
     descarta elementos emitidos por un Observable después de que una condición especificada se vuelva falsa
     **/
    public static void main(String[] args) {
        LOGGER.info("TakeWhile Operator");
        Observable<Integer> positiveNumbers = Observable.fromIterable(RxUtils.postiveNumbers(10));
        positiveNumbers.takeWhile(integer->integer<4).subscribe(new DemoObserver());

    }
}
