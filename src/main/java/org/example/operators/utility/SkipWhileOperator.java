package org.example.operators.utility;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.concurrent.TimeUnit;

public class SkipWhileOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(SkipWhileOperator.class);

    /**
     descarta los elementos emitidos por un Observable hasta que una condición especificada se vuelva falsa
     **/
    public static void main(String[] args) {
        LOGGER.info("SkipWhile Operator");
        Observable<Integer> positiveNumbers = Observable.fromIterable(RxUtils.postiveNumbers(10));
        positiveNumbers.skipWhile(integer->integer<4).subscribe(new DemoObserver());

    }
}
