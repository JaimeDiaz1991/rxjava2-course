package org.example.operators.utility;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

public class TimestampOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(TimestampOperator.class);

    /**
     adjunta una marca de tiempo a cada elemento emitido por un Observable
     **/
    public static void main(String[] args) {
        LOGGER.info("Timestamp Operator");
        Observable.fromIterable(RxUtils.shapes(10))
                .timestamp().subscribe(new DemoObserver());

    }
}
