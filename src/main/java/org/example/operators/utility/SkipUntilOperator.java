package org.example.operators.utility;

import io.reactivex.Observable;
import io.reactivex.SingleObserver;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.concurrent.TimeUnit;

public class SkipUntilOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(SkipUntilOperator.class);

    /**
     descarta elementos emitidos por un Observable hasta que un segundo Observable emita un elemento
     **/
    public static void main(String[] args) {
        LOGGER.info("SkipUntil Operator");
        Observable singleSecond = Observable.interval(1, TimeUnit.SECONDS);
        Observable fiiveSecond = Observable.interval(5, TimeUnit.SECONDS);
        singleSecond.skipUntil(fiiveSecond).subscribe(new DemoObserver());

    }
}
