package org.example.operators.utility;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class TimeoutOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(TimeoutOperator.class);

    /**
     Refleja la fuente Observable, pero emite una notificación de error si transcurre un período de tiempo en particular sin ningún elemento emitido
     **/
    public static void main(String[] args) {
        LOGGER.info("Timeout Operator");
        Observable.timer(2,TimeUnit.SECONDS)
                .timeout(1, TimeUnit.SECONDS).subscribe(new DemoObserver());
        RxUtils.sleep(3000L);

    }
}
