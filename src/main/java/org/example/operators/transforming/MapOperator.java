package org.example.operators.transforming;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observables.GroupedObservable;
import io.reactivex.schedulers.Schedulers;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.model.Shape;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

public class MapOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(MapOperator.class);

    /**
     * Se puede utilizar el map operador para asignar los valores de una corriente a diferentes valores
     * basados en los resultados para cada valor de la función pasada a map .
     * El flujo de resultados es una nueva copia y no modificará el flujo de valores proporcionado,
     * el flujo de resultados tendrá la misma longitud del flujo de entrada, pero puede ser de diferentes tipos.
     * La función pasada a .map() , debe devolver un valor.
     *
     **/
    public static void main(String[] args) {
        Observable.fromIterable(RxUtils.postiveNumbers(10))
                .map(integer -> (integer*2))
                .subscribe(new DemoObserver());
    }
    }
