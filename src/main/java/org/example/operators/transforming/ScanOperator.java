package org.example.operators.transforming;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

public class ScanOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScanOperator.class);

    /**
     * El operador de escaneo aplica una función al primer elemento emitido por la fuente Observable
     * y luego emite el resultado de esa función como su propia primera emisión.
     * También retroalimenta el resultado de la función en la función junto con el segundo elemento emitido
     * por la fuente Observable para generar su segunda emisión.
     * Continúa retroalimentando sus propias emisiones posteriores junto con las posteriores emisiones de la fuente Observable
     * para crear el resto de su secuencia.
     * Este tipo de operador a veces se denomina "acumulador" en otros contextos.
     **/
    public static void main(String[] args) {
        Observable.fromIterable(RxUtils.postiveNumbers(5))
                .scan((integer1,integer2) -> {return integer1+integer2;})
                .subscribe(new DemoObserver());
    }

}
