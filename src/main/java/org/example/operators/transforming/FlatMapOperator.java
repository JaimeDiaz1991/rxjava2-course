package org.example.operators.transforming;

import io.reactivex.Emitter;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

public class FlatMapOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(FlatMapOperator.class);

    /**
     El operador flatMap ayuda a transformar un evento en otro Observable (o transformar un evento en cero, uno o más eventos).
     Es un operador perfecto cuando desea llamar a otro método que devuelve un Observable
     *
     **/
    public static void main(String[] args) {
        Observable.fromIterable(RxUtils.postiveNumbers(10))
                .flatMap(integer -> {return twice(integer);})
                .subscribe(new DemoObserver());
    }

    public static Observable<Integer> twice(Integer integer){
        return Observable.create(observableEmitter -> {
            if (!observableEmitter.isDisposed()){
                observableEmitter.onNext(integer*2);
            }else{
                observableEmitter.onComplete();
            }

        });
    }

}
