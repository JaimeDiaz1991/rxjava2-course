package org.example.operators.transforming;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observables.GroupedObservable;
import io.reactivex.schedulers.Schedulers;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.model.Shape;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.concurrent.TimeUnit;

public class GroupByOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupByOperator.class);

    /**
     * Puedes crear emisiones agrupadas usando el operador groupBy().
     *
     **/
    public static void main(String[] args) {
        Observable.fromIterable(RxUtils.shapes(20))
                .groupBy(new Function<Shape, Object>() {
                    // Agrupar por forma/shape
                    @Override
                    public Object apply(@NonNull Shape shape) {
                        return shape.getShape();
                    }
                })
                .observeOn(Schedulers.newThread())
                .subscribe(new Observer<GroupedObservable<Object, Shape>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        LOGGER.info("onSubscribe");
                    }

                    @Override
                    public void onNext(@NonNull GroupedObservable<Object, Shape> objectShapeGroupedObservable) {
                        LOGGER.info("Key -> "+ objectShapeGroupedObservable.getKey());
                        //subscribirse a los elementos de cada grupo
                        objectShapeGroupedObservable.subscribe(new DemoObserver());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        LOGGER.info("onError -> "+ e.getMessage());

                    }

                    @Override
                    public void onComplete() {
                        LOGGER.info("onComplete");

                    }
                });
        RxUtils.sleep(99999L);
    }
    }
