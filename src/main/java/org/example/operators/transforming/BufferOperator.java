package org.example.operators.transforming;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.concurrent.TimeUnit;

public class BufferOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(BufferOperator.class);

    /**
     * Puedes crear emisiones agrupadas usando el operador buffer().
     *
     **/
    public static void main(String[] args) {
        bufferWithItems();
        LOGGER.info("\n******************************************************\n");
        bufferWithSeconds();
    }
    /**
     * Puedes crear emisiones agrupadas usando el operador buffer().
     * Aquí, utilizamos buffer() para agrupar todos los ítems en un grupos de tres items:
     *
     **/
    private static void bufferWithItems() {
        Observable.fromIterable(RxUtils.shapes(10))
                .buffer(3)
                .subscribe(new DemoObserver());
    }

    /**
     * Aquí, utilizamos buffer() para agrupar todos los ítems en un período de tres segundos:
     *
     **/
    public static void bufferWithSeconds(){
        Observable.fromIterable(RxUtils.shapes(10))
                .buffer(3, TimeUnit.SECONDS)
                .subscribe(new DemoObserver());
    }
}
