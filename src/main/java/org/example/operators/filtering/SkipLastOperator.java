package org.example.operators.filtering;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

public class SkipLastOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(SkipLastOperator.class);
    private static List<Character> changeIt= new ArrayList<>();

    /**
     * Suprimir los últimos n elementos emitidos por un Observable
     * Puede ignorar los últimos n elementos emitidos por un Observable y atender solo a los elementos que vienen después,
     * modificando el Observable con el operador Skip.
     **/
    public static void main(String[] args) {
        LOGGER.info("SkipLast Operator");
        Observable.fromIterable(RxUtils.postiveNumbers(10))
                .skipLast(4)
                .subscribe(new DemoObserver());

    }
}
