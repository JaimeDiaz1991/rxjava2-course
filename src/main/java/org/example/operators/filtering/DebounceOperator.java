package org.example.operators.filtering;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class DebounceOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DebounceOperator.class);
    private static List<Character> changeIt= new ArrayList<>();

    /**
     * solo emite un elemento de un Observable si ha pasado un período de tiempo en particular sin que emita otro elemento
     * Filtra los elementos emitidos por la fuente Observable que son seguidos rápidamente por otro elemento emitido.
     **/
    public static void main(String[] args) {
        LOGGER.info("Debounce Operator");
        Random r = new Random();
        Observable.interval(2,TimeUnit.SECONDS)
                .map(c -> {
                    changeIt.add((char)(r.nextInt(26)+'a'));
                    return changeIt;
                }).doOnEach(notification->{
                    LOGGER.info("Current Value -> "+ notification);
        })
                .debounce(2,TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .subscribe(new DemoObserver());
        RxUtils.sleep(10000L);
    }
}
