package org.example.operators.filtering;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

public class TakeOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(TakeOperator.class);
    private static List<Character> changeIt= new ArrayList<>();

    /**
     * Emite solo los primeros n elementos emitidos por un Observable
     **/
    public static void main(String[] args) {
        LOGGER.info("Take Operator");
        Observable.fromIterable(RxUtils.postiveNumbers(10))
                .take(4)
                .subscribe(new DemoObserver());

    }
}
