package org.example.operators.filtering;

import io.reactivex.CompletableObserver;
import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

public class SkipOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(SkipOperator.class);
    private static List<Character> changeIt= new ArrayList<>();

    /**
     * Suprimir los primeros n elementos emitidos por un Observable
     * Puede ignorar los primeros n elementos emitidos por un Observable y atender solo a los elementos que vienen después,
     * modificando el Observable con el operador Skip.
     **/
    public static void main(String[] args) {
        LOGGER.info("Skip Operator");
        Observable.fromIterable(RxUtils.postiveNumbers(10))
                .skip(5)
                .subscribe(new DemoObserver());

    }
}
