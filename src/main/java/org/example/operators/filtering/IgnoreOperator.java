package org.example.operators.filtering;

import io.reactivex.CompletableObserver;
import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

public class IgnoreOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(IgnoreOperator.class);
    private static List<Character> changeIt= new ArrayList<>();

    /**
     * no emite ningún elemento de un Observable pero refleja su notificación de terminación
     * El operador IgnoreElements suprime todos los elementos emitidos por la fuente Observable,
     * pero permite que su notificación de terminación (ya sea onError u onCompleted) pase sin cambios.
     *
     * Si no le importa que los elementos sean emitidos por un Observable,
     * pero desea que se le notifique cuando se complete o cuando termine con un error,
     * puede aplicar el operador ignoreElements al Observable,
     * lo que asegurará que nunca llamará los controladores onNext de sus observadores.
     **/
    public static void main(String[] args) {
        LOGGER.info("IgnoreElements Operator");
        Observable.fromIterable(RxUtils.postiveNumbers(10))
                .ignoreElements()
                .subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                LOGGER.info("onSubscribe");
            }

            @Override
            public void onError(@NonNull Throwable e) {
                LOGGER.info("onError --> "+ e.getMessage());
            }

            @Override
            public void onComplete() {
                LOGGER.info("onComplete");
            }
        });

    }
}
