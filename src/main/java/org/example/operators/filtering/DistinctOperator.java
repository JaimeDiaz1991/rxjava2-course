package org.example.operators.filtering;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class DistinctOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DistinctOperator.class);
    private static List<Character> changeIt= new ArrayList<>();

    /**
     * El operador Distinct filtra un Observable solo permitiendo el paso de elementos que aún no se han emitido.
     **/
    public static void main(String[] args) {
        LOGGER.info("Distinct Operator");
        Observable.just(1,2,3,4,1,2,3,4)
                .distinct().subscribe(new DemoObserver());
    }
}
