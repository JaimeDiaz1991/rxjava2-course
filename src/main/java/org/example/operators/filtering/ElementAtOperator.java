package org.example.operators.filtering;

import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

public class ElementAtOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ElementAtOperator.class);
    private static List<Character> changeIt= new ArrayList<>();

    /**
     * El operador ElementAt extrae un elemento ubicado en una ubicación de índice específica
     * en la secuencia de elementos emitidos por la fuente Observable y emite ese elemento como su única emisión.
     **/
    public static void main(String[] args) {
        LOGGER.info("ElementAt Operator");
        Observable.fromIterable(RxUtils.postiveNumbers(10))
                .elementAt(4).subscribe(new MaybeObserver<Integer>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                LOGGER.info("onSubscribe");
            }

            @Override
            public void onSuccess(@NonNull Integer integer) {
                LOGGER.info("onSuccess --> "+ integer);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                LOGGER.info("onError --> "+ e.getMessage());
            }

            @Override
            public void onComplete() {
                LOGGER.info("onComplete");
            }
        });

    }
}
