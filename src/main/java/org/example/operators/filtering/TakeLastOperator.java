package org.example.operators.filtering;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

public class TakeLastOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(TakeLastOperator.class);
    private static List<Character> changeIt= new ArrayList<>();

    /**
     * Emite solo los últimos n elementos emitidos por un Observable
     **/
    public static void main(String[] args) {
        LOGGER.info("TakeLast Operator");
        Observable.fromIterable(RxUtils.postiveNumbers(10))
                .takeLast(4)
                .subscribe(new DemoObserver());

    }
}
