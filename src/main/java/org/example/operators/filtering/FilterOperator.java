package org.example.operators.filtering;

import io.reactivex.Observable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.model.Shape;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

public class FilterOperator {
    private static final Logger LOGGER = LoggerFactory.getLogger(FilterOperator.class);
    private static List<Character> changeIt= new ArrayList<>();

    /**
     * emitir solo aquellos elementos de un Observable que pasan una prueba de predicado
     **/
    public static void main(String[] args) {
        LOGGER.info("Filter Operator");
        List<Shape> shapes = RxUtils.shapes(10);
        for (Shape shape: shapes){
            LOGGER.info("shape --> " + shape);
        }
        Observable.fromIterable(shapes)
                .filter(shape -> shape.getColor().equals("blue"))
                .subscribe(new DemoObserver());

    }
}
