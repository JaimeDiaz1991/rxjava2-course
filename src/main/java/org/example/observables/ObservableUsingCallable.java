package org.example.observables;

import io.reactivex.Observable;
import org.example.observer.DemoObserver;

import java.util.concurrent.Callable;

/*
 * Observable.
 * 1. Emit a stream elements (endlessly/ sin fin)
 * */
public class ObservableUsingCallable {

    /*
    * 1.
    * */

    public static void main(String[] args) {

        Observable.fromCallable(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return doSomething();
            }
        }).subscribe(new DemoObserver());
    }

    public static String doSomething(){
        try {
            Thread.sleep(2000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        return "HOLA";
    }
}
