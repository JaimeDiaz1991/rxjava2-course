package org.example.observables;

import io.reactivex.Observable;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.concurrent.TimeUnit;

/*
 * Observable.
 * 1. Emit a stream elements (endlessly/ sin fin)
 * */
public class ObservableUsingRange {
    /*
    * 1. emits range of elements
    * 2. takes 2 arguments starting number and range
    * */

    public static void main(String[] args) {
    Observable.range(1, 10).subscribe(new DemoObserver());
    }
}
