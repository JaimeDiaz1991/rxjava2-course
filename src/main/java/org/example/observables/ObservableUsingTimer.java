package org.example.observables;

import io.reactivex.Observable;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.concurrent.TimeUnit;

/*
 * Observable.
 * 1. Emit a stream elements (endlessly/ sin fin)
 * */
public class ObservableUsingTimer {
    /*
    * 1 emits events after the specified time
    * */

    public static void main(String[] args) {
    Observable.timer(5, TimeUnit.SECONDS).subscribe(new DemoObserver());
    RxUtils.sleep(10000L);
    }
}
