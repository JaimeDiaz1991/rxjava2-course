package org.example.observables;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import org.example.model.Shape;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.List;
import java.util.concurrent.Callable;

/*
 * Observable.
 * 1. Emit a stream elements (endlessly/ sin fin)
 * */
public class ObservableUsingDefer {
    /*
    * 1 create new observable every time on subscription
    * 2 No observable till subscription
    * */

    public static void main(String[] args) {
    Observable<Integer> observable = Observable.defer(()->{
        return Observable.fromIterable(RxUtils.postiveNumbers(5));
    });
    observable.subscribe(new DemoObserver());
    observable.subscribe(new DemoObserver());
    }
}
