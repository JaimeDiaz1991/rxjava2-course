package org.example.observables;

import io.reactivex.Observable;
import org.example.observer.DemoObserver;
/*
 * Observable.
 * 1. Emit a stream elements (endlessly/ sin fin)
 * */
public class ObservableUsingJust {

    public static void main(String[] args) {
        Observable.just("a","b","c","d","e","f","g","h","i","j")
                .subscribe(new DemoObserver());
    }
}
