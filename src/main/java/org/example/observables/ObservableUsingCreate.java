package org.example.observables;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.vertx.reactivex.ext.web.handler.CSRFHandler;
import org.example.model.Shape;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.List;

/*
 * Observable.
 * 1. Emit a stream elements (endlessly/ sin fin)
 * */
public class ObservableUsingCreate {


    public static void main(String[] args) {
    List<Shape> shapes = RxUtils.shapes(15);
        Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> objectObservableEmitter)throws Exception{
                try {
                    shapes.forEach(objectObservableEmitter::onNext);
                }catch (Exception e){
                    objectObservableEmitter.onError(e);
                }
            objectObservableEmitter.onComplete();
            }
        }).subscribe(new DemoObserver());
    }
}
