package org.example.observables;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.concurrent.atomic.AtomicInteger;

public class ObservableWithBackPressure {

    private static final Logger LOGGER = LoggerFactory.getLogger(ObservableWithBackPressure.class);

    /*
    * Si configuramos en la ejecución las VMOptions con -Xmx32m
    * El programa no fallará gracias al backPressure
     * */
    public static void main(String[] args) {
        Flowable<Integer> integerFlowable = Flowable.fromIterable(RxUtils.postiveNumbers(1000000))
                .repeat()
                .observeOn(Schedulers.newThread(),false,5)
                .subscribeOn(Schedulers.newThread())
                .doOnNext(integer -> LOGGER.info("emmiting integer -> " + integer));
        integerFlowable.subscribe(new Subscriber<Integer>() {
            private Subscription subscription;
            private AtomicInteger counter = new AtomicInteger(0);
            @Override
            public void onSubscribe(Subscription s) {
                LOGGER.info("onSubscribe");
                this.subscription = s;
                s.request(5);
            }

            @Override
            public void onNext(Integer integer) {
                LOGGER.info("onNext -> "+integer);
                RxUtils.sleep(100L);
                if (counter.incrementAndGet()%5==0)
                subscription.request(5);
            }

            @Override
            public void onError(Throwable t) {
                LOGGER.info("onError "+ t.getMessage());

            }

            @Override
            public void onComplete() {
                LOGGER.info("onComplete");

            }
        });
        RxUtils.sleep(10000L);
    }
}
