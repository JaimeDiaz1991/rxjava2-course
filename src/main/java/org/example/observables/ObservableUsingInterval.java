package org.example.observables;

import io.reactivex.Observable;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

import java.util.concurrent.TimeUnit;

/*
 * Observable.
 * 1. Emit a stream elements (endlessly/ sin fin)
 * */
public class ObservableUsingInterval {
    /*
    * 1 emits sequence o integer at regular interval
    * 2 Ideal for repetitive job
    * */

    public static void main(String[] args) {
    Observable.interval(1, TimeUnit.SECONDS).subscribe(new DemoObserver());
    RxUtils.sleep(3000L);
    }
}
