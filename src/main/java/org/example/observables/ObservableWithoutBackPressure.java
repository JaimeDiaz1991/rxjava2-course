package org.example.observables;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.example.observer.DemoObserver;
import org.example.utils.RxUtils;

public class ObservableWithoutBackPressure {

    private static final Logger LOGGER = LoggerFactory.getLogger(ObservableWithoutBackPressure.class);

    /*
    * Si configuramos en la ejecución las VMOptions con -Xmx32m
    * El programa fallará: java.lang.OutOfMemoryError: Java heap space
     * */
    public static void main(String[] args) {
        Observable<Integer> positiveNumberEvents = Observable.fromIterable(RxUtils.postiveNumbers(1000000))
                .repeat()
                .observeOn(Schedulers.newThread())
                .subscribeOn(Schedulers.newThread());
        positiveNumberEvents.subscribe(new DemoObserver());
        RxUtils.sleep(100000L);
    }
}
