package org.example.observer;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class DemoObserver implements Observer {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoObserver.class);
    @Override
    public void onSubscribe(@NonNull Disposable d) {
        LOGGER.info("onSubscribe");
    }

    @Override
    public void onNext(@NonNull Object s) {
        LOGGER.info("onNext -> "+ s);
    }

    @Override
    public void onError(@NonNull Throwable e) {
        LOGGER.info("onError -> " + e.getMessage());
    }

    @Override
    public void onComplete() {
        LOGGER.info("onComplete");
    }
}
